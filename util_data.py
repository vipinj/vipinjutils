from google.appengine.ext import db
import datetime

class util_data(db.Model):
    """Stores the utility data"""
    email = db.TextProperty()
    amount = db.FloatProperty(required=True)
    billdate = db.DateProperty(required=True)
    recddate = db.DateProperty(auto_now=True)


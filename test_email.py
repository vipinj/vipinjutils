import logging, email

from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler 
from google.appengine.ext.webapp.util import run_wsgi_app

import util_data
import date_func

class MainPage(webapp.RequestHandler):
    def get(self):
#        self.response.headers['Content-Type'] = 'text/plain'
#        self.response.out.write('Hello, webapp World!')
        q = db.GqlQuery("SELECT * from util_data ORDER BY billdate")
        results = q.fetch(100)
#        self.response.headers['Content-Type'] = 'text/html'
        self.response.out.write("""
  <html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Year');
        data.addColumn('number', 'National Grid');
        data.addRows([
""")
        for p in results:
            self.response.out.write('[\'%s\', %s],\n' % (date_func.date_func(str(p.billdate)), p.amount))
        self.response.out.write("""
        ]);

        var options = {
          title: 'Gas Bill(Bills mostly received at the end of the month'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
  </body>
</html>""")

application = webapp.WSGIApplication(
                                     [('/', MainPage),
                                      ],
                                     debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
    
